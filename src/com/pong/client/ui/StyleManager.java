/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.pong.client.Pong;

/**
 *
 * @author Clem
 */
public class StyleManager {
    
    private Pong pong;
    private LabelStyle lstyle;
    private CheckBoxStyle cstyle;
    private ScrollPaneStyle sstyle;
    
    public StyleManager(Pong p)
    {
        pong = p;
    }
    
    public void load()
    {
        cstyle = new CheckBoxStyle();
        cstyle.checkboxOff = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.local("ressource/ui/checkbox/check-off.png"))));
        cstyle.checkboxOn = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.local("ressource/ui/checkbox/check-on.png"))));
        cstyle.font = pong.getFont("bebas-little");
        
        sstyle = new ScrollPaneStyle();
        
        Texture scroll = new Texture(Gdx.files.local("ressource/ui/scroll/scroll.png"));
        //scroll.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        scroll.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        
        Texture bg = new Texture(Gdx.files.local("ressource/ui/scroll/bg.png"));
        //bg.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bg.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        
        TextureRegion t = new TextureRegion(scroll);
        
        //sstyle.vScroll = new TextureRegionDrawable(new TextureRegion(bg));
        //sstyle.vScrollKnob = new TextureRegionDrawable(new TextureRegion(scroll));
        //sstyle.vScrollKnob= new TextureRegionDrawable(new TextureRegion(scroll));
        //sstyle.hScroll = new TextureRegionDrawable(new TextureRegion(bg));
        //sstyle.hScrollKnob = new TextureRegionDrawable(new TextureRegion(bg));
    }
    
    public ScrollPaneStyle scrollStyle()
    {
        return sstyle;
    }
    
    public CheckBoxStyle checkBox()
    {
        return cstyle;
    }
    
}
