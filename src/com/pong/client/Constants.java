/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client;

/**
 *
 * @author Clem
 */
public class Constants {
    
    public final static int loading   = 0;
    public final static int mainMenu  = 1;
    public final static int game      = 2;
    public final static int win       = 3;
    public final static int lose      = 4;
    public final static int settings  = 5;
    public final static int textures  = 6;
    
}
