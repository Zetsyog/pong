/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pong.client.event.Event;
import com.pong.client.screen.GuiScreen;
import com.pong.client.screen.LoadingScreen;
import com.pong.client.settings.GameSettings;
import com.pong.client.texture.TextureManager;
import com.pong.client.ui.StyleManager;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clem
 */
public class Pong extends Game{

    private final String version = "0.7.2";
    private final int nbr_version = 3;
    public static Pong instance;
    public final InputMultiplexer multiplexer;
    private HashMap<String, String> params;
    private HashMap<String, BitmapFont> fonts;
    private HashMap<Integer, GuiScreen> screens;
    public SpriteBatch batch;
    private Event input;
    public final TextureManager textureManager;
    public final GameSettings gameSettings;
    public final StyleManager styleManager;
    
    public Pong()
    {
        params = new HashMap<String, String>();
        screens = new HashMap<Integer, GuiScreen>();
        fonts = new HashMap<String, BitmapFont>();
        multiplexer = new InputMultiplexer();
        gameSettings = new GameSettings(this);
        textureManager = new TextureManager(this);
        styleManager = new StyleManager(this);
    }
    
    public String getVersion()
    {
        return version;
    }
    
    public int getNumberVersion()
    {
        return nbr_version;
    }
    
    public BitmapFont getFont(String name)
    {
        return (fonts.get(name) != null) ? fonts.get(name) : fonts.get("default");
    }
    
    public void addScreen(GuiScreen s)
    {
        this.screens.put(s.getId(), s);
    }
    
    public GuiScreen getScreen(int id)
    {
        return this.screens.get(id);
    }
    
    public void addFont(String name, BitmapFont font)
    {
        fonts.put(name, font);
    }
    
    public void bindParam(String key, String value)
    {
        params.put(key, value);
    }
    
    public String getParam(String key)
    {
        return (params.get("--" + key))!= null ? params.get("--" + key) : "";
    }
    
    @Override
    public void create() {
        if(getParam("username") == null || getParam("username").equalsIgnoreCase(""))
        {
            bindParam("--username", "Player");
        }
        
        System.out.println("Username :" + getParam("username"));
        batch = new SpriteBatch();
        gameSettings.load();
        this.addFont("default", new BitmapFont());
        input = new Event();
        Gdx.input.setInputProcessor(multiplexer);
        this.addScreen(new LoadingScreen(this, Constants.loading));
        this.setScreen(getScreen(Constants.loading));
        Pong.instance = this;
        
    }
    
    public void loadTM()
    {
        textureManager.load();
    }

    public Event getInput()
    {
        return input;
    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        for(Map.Entry<Integer, GuiScreen> e : screens.entrySet())
        {
            e.getValue().dispose();
        }
        this.batch.dispose();
        try {
            gameSettings.save();
        } catch (IOException ex) {
            Logger.getLogger(Pong.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        super.dispose();
    }
    
}
