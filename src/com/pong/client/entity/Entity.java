/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;

/**
 *
 * @author Clem
 */
abstract public class Entity {
    protected float x;
    protected float y;
    protected float sx;
    protected ShapeRenderer r;
    protected float sy;
    protected Pong pong;
    protected SpriteBatch batch;
    
    public Entity(Pong p, float x, float y, float sx, float sy)
    {
        pong = p;
        this.x = x;
        this.y = y;
        this.sx = sx;
        this.sy = sy;
        batch = pong.batch;
        r = new ShapeRenderer();
    }
    
    abstract public void update(float f);
    abstract public void render(float f);
    
    public float getX()
    {
        return x;
    }
    
    public float getY()
    {
        return y;
    }
    
    public float getSX()
    {
        return sx;
    }
    public float getSY()
    {
        return sy;
    }
    
    public float getCenterY()
    {
        return y + sy / 2;
    }
    
    public void setY(float y)
    {
        this.y = y;
    }
    public void setX(float x)
    {
        this.x = x;
    }
    
    public void dispose()
    {
        
    }
}
