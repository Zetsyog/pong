/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;
import com.pong.client.engine.Physics;
import com.pong.client.texture.TexturePack;


/**
 *
 * @author Clem
 */
// Les murs
public class EntityWall extends Entity{

    public static final int STDSIZE = 16;
    
    private EntityBall ball;
    private Texture image;
    
    public EntityWall(Pong p, float x, float y, EntityBall ball)
    {
        super(p, x, y, Gdx.graphics.getWidth(), STDSIZE);
        this.ball = ball;
        image = new Texture(Gdx.files.local("ressource/image/wall.png"));
        image.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }
    
    public void setTexturePack(TexturePack p)
    {
        image = p.getWall();
    }
    
    @Override
    public void update(float f)
    {
        if(Physics.checkCollision(this, ball))
        {
            ball.reverseY();
            if(this.y <= 1)
            {
                ball.setY(y + sy + 1);
            }
            else
            {
                ball.setY(y - ball.getSY());
            }
        }
        this.sx = Gdx.graphics.getWidth();
        if(this.y != 0)
        {
            this.y = Gdx.graphics.getHeight()- this.sy;
        }
    }
    
    @Override
    public void render(float f) 
    {
        batch.begin();
            batch.draw(image, x, y, sx, sy);
        batch.end();
        
        r.begin(ShapeRenderer.ShapeType.Line);
            r.rect(x, y, sx, sy);
        r.end();
    }
    
    @Override
    public void dispose()
    {
        r.dispose();
        image.dispose();
    }
    
}
