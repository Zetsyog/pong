/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;
import com.pong.client.engine.Physics;
import com.pong.client.texture.TexturePack;

/**
 *
 * @author Clem
 */
public class EntityPlayer extends Entity{

    public static final int SIZEX = 16;
    public static final int SIZEY = SIZEX * 7;
    public static final float SPEED = 6.5f;
    public static final float MAX_SPEEDY = 6.0f;
    public static final float IA_SPEED = 0.3f;
    public static final float DAMPING = 0.08f;
    
    public boolean canMoveUp = true;
    public boolean canMoveDown = true;
    private EntityBall ball;
    private Sound sound;
    
    private String name;
    private boolean ia;
    private Texture t;
    
    public EntityPlayer(Pong p, int x, int y, EntityBall ball, String name, boolean ia)
    {
        super(p, x, y , SIZEX, SIZEY);
        this.ball = ball;
        this.name = name;
        t = new Texture(Gdx.files.local("ressource/image/player/default.png"));
        t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        this.ia = ia;
        
        if(ia)
        {
            sound = Gdx.audio.newSound(Gdx.files.internal("ressource/sound/pongBip2.wav"));
        }
        else
        {
            sound = Gdx.audio.newSound(Gdx.files.internal("ressource/sound/pongBip1.wav"));
        }
    }
    
    public void setTexturePack(TexturePack p)
    {
        if(!ia)
        {
            t = p.getPlayer();
        }
        else
        {
            t = p.getEnemy();
        }
    }
    
    public void move(float mag, float delta)
    {
        float speed = (SPEED * mag) * delta * 100;
        if(speed > MAX_SPEEDY) {
            speed = MAX_SPEEDY;
        }
        if(speed < -MAX_SPEEDY) {
            speed = -MAX_SPEEDY;
        }
        
        if(mag == -1)
        {
            if(canMoveDown)
            {
                y += speed;
            }else {}
        }
        else if(mag == 1)
        {
            if(canMoveUp)
            {
               y += speed;
            }else {}
        }
        
    }
    
    public String getName()
    {
        return name;
    }
    
    @Override
    public void update(float f)
    {
        if(Physics.checkCollision(this, ball))
        {
            ball.reverseX(this.getCenterY());
            if(this.x <= 1)
            {
                ball.setX(this.x + this.sx + 1);
            }
            else
            {
               ball.setX(this.x - ball.getSX());
            }
            
            long id = sound.play(1.0f);
        }
        
        if(!ia)
        {
            if(Gdx.input.isKeyPressed(pong.gameSettings.getKey("key.up")))
            {
                this.move(1, f);
            }
            else if(Gdx.input.isKeyPressed(pong.gameSettings.getKey("key.down")))
            {
                this.move(-1, f);
            }
        }
        else
        {
            float speed = ((ball.getCenterY() - getCenterY()) * DAMPING) * f * 100 ;
        
        
            if(speed > MAX_SPEEDY + IA_SPEED) {
                speed = MAX_SPEEDY + IA_SPEED;
            }
            if(speed < -MAX_SPEEDY - IA_SPEED) {
                speed = -MAX_SPEEDY - IA_SPEED;
            }
        
            if(speed < 0)
            {
                if(canMoveUp)
                {
                    y +=  speed;
                }
            }
            else if(speed > 0)
            {
                if(canMoveDown)
                {
                    y += speed;
                }
            }
        }
    }
    
    @Override
    public void render(float f) 
    {
        pong.batch.begin();
            pong.batch.draw(t, x, y, sx, sy);
        pong.batch.end();
        r.begin(ShapeRenderer.ShapeType.Line);
            r.setColor(Color.WHITE);
            this.r.rect(x, y, sx, sy);
            r.setColor(Color.CLEAR);
        r.end();
    }
    
    @Override
    public void dispose()
    {
        r.dispose();
        sound.dispose();
        t.dispose();
    }
    
}
