/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;
import com.pong.client.texture.TexturePack;

/**
 *
 * @author Clem
 */
public class EntityBall extends Entity{

    public static final int SIZE = 24;
    public static final float MAX_SPEEDX = 4.5f;
    public static final float MAX_SPEEDY = 7.5f;
    public static final float DAMPING = 0.09f;
    
    public float startX;
    public float startY;
    
    public float velX;
    public float velY;
    
    private Texture t;
    
    public EntityBall(Pong p, float x, float y)
    {
        super(p, x, y, SIZE, SIZE);
        t = new Texture(Gdx.files.local("ressource/image/ball.png"));
        this.startX = x;
        this.startY = y;
        
        velX = -MAX_SPEEDX;
        velY = 0;
    }
    
    public void setTexturePack(TexturePack p)
    {
        t = p.getBall();
    }
    
    @Override
    public void update(float f)
    {
        this.x += velX;
        this.y += velY;
    }
    
    @Override
    public void render(float f) 
    {
        
        batch.begin();
            batch.draw(t, x, y, sx, sy);
        batch.end();
        r.begin(ShapeRenderer.ShapeType.Line);
            r.circle(x + sx / 2, y + sy / 2, SIZE / 2);
        r.end();
    }

    public void reset() 
    {
        this.x = startX;
        this.y = startY;
        this.velY = 0;
        this.velX = -MAX_SPEEDX;
    }
    
    public void reverseX(float center)
    {
        velX *= -1;
        velY += (getCenterY() -  center) * DAMPING;
        
        if(velY > MAX_SPEEDY) {
            velY = MAX_SPEEDY;
        }
        
        if(velY < -MAX_SPEEDY) {
            velY = -MAX_SPEEDY;
        }
        
    }
    
    public void reverseY()
    {
        velY *= -1;
    }
    
    @Override
    public void dispose()
    {
        this.t.dispose();
        this.r.dispose();
    }
}
