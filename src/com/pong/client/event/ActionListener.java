/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.event;

import com.pong.client.Pong;
import com.pong.client.gui.ZButton;

/**
 *
 * @author Clem
 */
public interface ActionListener {
    public void actionPerformed(ZButton button, Pong pong);

}
