/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.pong.client.gui.GuiObject;


/**
 *
 * @author Clem
 */
public class Event{

    public boolean isMouseOver(GuiObject go) 
    {
        Rectangle r1 = new Rectangle(go.getX(), go.getY(), go.getSX(), go.getSY());
        Rectangle r2 = new Rectangle(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY() + go.getSY(), 1, 1);
        
        return r1.contains(r2);
    }

    

}
