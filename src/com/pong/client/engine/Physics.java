/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.engine;

import com.badlogic.gdx.math.Rectangle;
import com.pong.client.entity.Entity;

/**
 *
 * @author Clem
 */
public class Physics {
    // Juste une petite fonction pour checker les colision entre 2 GO
    public static boolean checkCollision(Entity go1, Entity go2)
    {
        Rectangle r1 = new Rectangle((int)go1.getX(), (int)go1.getY(), (int)go1.getSX(), (int)go1.getSY());
        Rectangle r2 = new Rectangle((int)go2.getX(), (int)go2.getY(), (int)go2.getSX(), (int)go2.getSY());
        return r1.overlaps(r2);
        
        
    }
    
    /*public static boolean checkCollision(Rectangle r, Entity go2)
    {
        Rectangle r2 = new Rectangle((int)go2.getX(), (int)go2.getY(), (int)go2.getSX(), (int)go2.getSY());
        return r.contains(r2);
    }*/
}
