/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.engine;

import com.badlogic.gdx.Gdx;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.entity.*;



/**
 *
 * @author Clem
 */
public class GameEngine {
    
    private final Pong pong;
    
    private EntityBall ball;
    private EntityPlayer player;
    private EntityPlayer enemy;
    private EntityWall wall1;
    private EntityWall wall2;
    
    private int playerScore = 0;
    private int enemyScore = 0;
    private int maxScore = 3;
    
    
    public GameEngine (EntityBall ball,EntityPlayer p1, EntityPlayer p2, EntityWall w1, EntityWall w2 )
    {
        pong = Pong.instance;
        this.ball = ball;
        this.player = p1;
        this.enemy = p2;
        this.wall1 = w1;
        this.wall2 = w2;
    }
    
    public int getPlayerScore()
    {
        return playerScore;
    }
    
    public int getEnemyScore()
    {
        return enemyScore;
    }
    
    public void dispose()
    {
        player.dispose();
        enemy.dispose();
        ball.dispose();
        wall1.dispose();
        wall2.dispose();
    }
    
    public void update(float f)
    {
        player.update(f);
        enemy.update(f);
        ball.update(f);
        wall1.update(f);
        wall2.update(f);
    }
    public void render(float f)
    {
        player.render(f);
        enemy.render(f);
        ball.render(f);
        wall1.render(f);
        wall2.render(f);
        
        // Joueur
        if(player.getY() + player.getSY() >= Gdx.graphics.getHeight() - EntityWall.STDSIZE)
        {
            player.canMoveDown = true;
            player.canMoveUp = false;
        }
        else
        {
            player.canMoveUp = true;
        }
        
        if(player.getY() <= EntityWall.STDSIZE)
        {
            player.canMoveUp = true;
            player.canMoveDown = false;
        }
        else
        {
            player.canMoveDown = true;
        }
        
        // Ennemi
        if(enemy.getY() >= Gdx.graphics.getHeight() - EntityWall.STDSIZE - enemy.getSY())
        {
            enemy.canMoveDown = false;
            enemy.canMoveUp = true;
        }
        else
        {
            enemy.canMoveDown = true;
        }
        
        if(enemy.getY() <= EntityWall.STDSIZE)
        {
            enemy.canMoveUp = false;
            enemy.canMoveDown = true;
        }
        else
        {
            enemy.canMoveUp = true;
        }
        
        
        if(ball.getX() < 0)
        {
            enemyScore++;
            ball.reset();
        }
        
        if(ball.getX() + ball.getSX() > Gdx.graphics.getWidth())
        {
            playerScore++;
            ball.reset();
        }
        
        if(maxScore > 0)
        {
            if(playerScore >= maxScore)
            {
                pong.setScreen(pong.getScreen(Constants.win));
                this.reset();
            }
            else if(enemyScore >= maxScore)
            {
                pong.setScreen(pong.getScreen(Constants.lose));
                this.reset();
            }
        }
        else
        {
            if(enemyScore >= 1)
            {
                pong.setScreen(pong.getScreen(Constants.lose));
                this.reset();
            }
        }
    }
    
    public void setMaxScore(int i)
    {
        maxScore = i;
    }
    
    public void reset()
    {
        playerScore = 0;
        enemyScore = 0;
        ball.reset();
        player.setY(Gdx.graphics.getHeight() / 2 - EntityPlayer.SIZEY / 2);
        enemy.setY(Gdx.graphics.getHeight() / 2 - EntityPlayer.SIZEY / 2);
    }
    
}
