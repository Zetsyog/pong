/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.util;

import java.io.File;

/**
 *
 * @author Clem
 */
public class Util {
    public static File getWorkingDirectory(String applicationName) 
    {
        String userHome = System.getProperty("user.home", ".");
        File workingDirectory;
        if(getPlatform() == OS.solaris || getPlatform() ==  OS.linux)
        {
            workingDirectory = new File(userHome, "ZGames/" + applicationName + '/');
        }
        else if(getPlatform() == OS.windows)
        {
            String applicationData = System.getenv("APPDATA");
            if (applicationData != null) {
                workingDirectory = new File(applicationData, "ZGames/" + applicationName + '/');
            } else {
                workingDirectory = new File(userHome, "ZGames/" + applicationName + '/');
            }
        }
        else if(getPlatform() == OS.macosx)
        {
            workingDirectory = new File(userHome, "Library/Application Support/" + "ZGames" + applicationName + '/');
        }
        else
        {
            workingDirectory = new File(userHome, "ZGames/" + applicationName + '/');
        }

        if ((!workingDirectory.exists()) && (!workingDirectory.mkdirs())) {
            throw new RuntimeException("The working directory could not be created: " + workingDirectory);
        }
        return workingDirectory;
    }

    public static OS getPlatform() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            return OS.windows;
        }
        if (osName.contains("mac")) {
            return OS.macosx;
        }
        if (osName.contains("solaris")) {
            return OS.solaris;
        }
        if (osName.contains("sunos")) {
            return OS.solaris;
        }
        if (osName.contains("linux")) {
            return OS.linux;
        }
        if (osName.contains("unix")) {
            return OS.linux;
        }
        return OS.unknown;
    }
    
    public static enum OS
    {
        linux, solaris, windows, macosx, unknown;
    }
}
