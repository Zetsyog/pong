/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.util;

import static com.badlogic.gdx.Input.Keys.*;
import com.badlogic.gdx.Input;

/**
 *
 * @author Clem
 */
public class InputUtil {
    protected InputUtil()
    {}
    
    public static String getKeyName(int i)
    {
        switch(i)
        {
            case A: return "A";
            case B: return "B";
            case C: return "C";
            case D: return "D";
            case E: return "E";
            case F: return "F";
            case G: return "G";
            case H: return "H";
            case I: return "I";
            case J: return "J";
            case K: return "K";
            case L: return "L";
            case M: return "M";
            case N: return "N";
            case O: return "O";
            case P: return "P";
            case Q: return "Q";
            case R: return "R";
            case S: return "S";
            case T: return "T";
            case U: return "U";
            case V: return "V";
            case W: return "W";
            case X: return "X";
            case Y: return "Y";
            case Z: return "Z";
            case UP:     return "UP";
            case DOWN:   return "DOWN";
            case LEFT:   return "LEFT";
            case RIGHT:  return "RIGHT";
            default:
                return "Unknown";
        }
    }
}
