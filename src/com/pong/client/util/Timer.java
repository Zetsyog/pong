/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.util;

import com.pong.client.Pong;

/**
 *
 * @author Clem
 */
public class Timer {
    
    private int millisec = 0;
    private int dixsec = 0;
    private int sec = 0;
    private int min = 0;
    private int time = 0;
    private boolean pause = false;
    
    public void update(float delta)
    {
        if(!pause)
        {
            int i = (int) (1000 * delta);
            millisec += 10 * i;
            
            if(millisec >= 1000)
            {
                millisec = 0;
                dixsec++;
                time++;
            }
            if(dixsec >= 10)
            {
                dixsec = 0;
                sec++;             
            }

            if(sec == 60)
            {
                sec = 0;
                min++;
            }
        }
    }
    
    public void setPause(boolean b)
    {
        pause = b;
    }
    
    public int getMilli()
    {
        return millisec;
    }
    
    public int getDix()
    {
        return dixsec;
    }
    
    public int getSec()
    {
        return sec;
    }
    
    public int getMin()
    {
        return min;
    }
    
    public int getTime()
    {
        return time;
    }
    
    public void reset()
    {
        millisec = 0;
        dixsec = 0;
        sec = 0;
        min = 0;
        time = 0;
    }
    
}
