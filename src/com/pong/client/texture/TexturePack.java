/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.texture;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clem
 */
public class TexturePack {
    private Texture ball, wall, player, enemy, background;
    private File info;
    private HashMap<String, String> properties;
    
    
    public TexturePack()
    {
        properties = new HashMap<String, String>();
    }
    
    public void setDefaultTextures()
    {
        player = new Texture(Gdx.files.local("ressource/image/player/default.png"));
        enemy = new Texture(Gdx.files.local("ressource/image/player/default.png"));
        wall = new Texture(Gdx.files.local("ressource/image/wall.png"));
        ball = new Texture(Gdx.files.local("ressource/image/ball.png"));
        background = new Texture(Gdx.files.local("ressource/image/background.png"));
        
        this.properties.put("name", "default");
        this.properties.put("version", "none");
        this.properties.put("author", "Zetsyog");
        this.properties.put("description", "Default Textures");
        this.setFilters();
    }
    
    public void setFilters()
    {
        if(player != null)
        {
            player.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        else if(enemy != null)
        {
            enemy.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        else if(wall != null)
        {
            wall.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        else if(ball != null)
        {
            ball.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        else if(background != null)
        {
            background.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }
    
    public String getPropertie(String key)
    {
        return this.properties.get(key) != null ? properties.get(key) : "unset";
    }
    
    private boolean loadInfo()
    {
        try {
            BufferedReader bis = new BufferedReader(new FileReader(info));
            String line;
            boolean r = true;
            
            while((line = bis.readLine()) != null)
            {
                if(line.contains(": "))
                {
                    String[] str = line.split(": ");
                    if(str.length == 2)
                    {
                        if(line.startsWith("name: "))
                        {
                            this.properties.put(str[0], str[1]);
                        }
                        else if(line.startsWith("author: "))
                        {
                            this.properties.put(str[0], str[1]);
                        }
                        else if(line.startsWith("version: "))
                        {
                            this.properties.put(str[0], str[1]);
                        }
                        else if(line.startsWith("description: "))
                        {
                            this.properties.put(str[0], str[1]);
                        }
                        else
                        {
                            r = false;
                        }
                    }
                }
            }
            return r;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TexturePack.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TexturePack.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean setInfo(File f)
    {
        this.info = f;
        return this.loadInfo();
    }
    
    public void setPlayer(FileHandle f)
    {
        player = new Texture(f);
        this.setFilters();
    }
    
    public void setBall(FileHandle f)
    {
        ball = new Texture(f);
        this.setFilters();
    }
    
    public void setWall(FileHandle f)
    {
        wall = new Texture(f);
        this.setFilters();
    }
    public void setEnemy(FileHandle f)
    {
        enemy = new Texture(f);
        this.setFilters();
    }
    public void setBackground(FileHandle f)
    {
        background = new Texture(f);
        this.setFilters();
    }
    public Texture getPlayer()
    {
        return player;
    }
    
    public Texture getBall()
    {
        return ball;
    }
    
    public Texture getWall()
    {
        return wall;
    }
    public Texture getEnemy()
    {
        return enemy != null ? enemy : player;
    }
    public Texture getBackground()
    {
        return background;
    }
    
    
}
