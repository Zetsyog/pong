/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.texture;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.screen.GuiTextures;
import com.pong.client.util.FileUtil;
import com.pong.client.util.Util;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author Clem
 */
public class TextureManager {
    
    private Pong pong;
    private File textureFolder;
    private File[] textures;
    private TexturePack currentTexture;
    private HashMap<String, TexturePack> textureList;
    
    
    
    public TextureManager(Pong p)
    {
        pong = p;
        textureList = new HashMap<String, TexturePack>();
    }
    
    
    
    public void load()
    {
        if(!pong.getParam("working-dir").equalsIgnoreCase(""))
        {
            textureFolder = new File(pong.getParam("working-dir") + "/texturepacks");
        }
        else
        {
            textureFolder = new File(Util.getWorkingDirectory("Pong/texturepacks").getPath());
        }
        
        
        
        if(!textureFolder.exists())
        {
            textureFolder.mkdirs();
            this.currentTexture = new TexturePack();
            this.currentTexture.setDefaultTextures();
        }
        
        textures = textureFolder.listFiles();
        
        try {
            this.loadTexturePacks();
            
        } catch (IOException ex) {
            Logger.getLogger(TextureManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void loadTexturePacks() throws IOException
    {
        TexturePack d = new TexturePack();
        d.setDefaultTextures();
        
        this.textureList.clear();
        this.textureList.put(d.getPropertie("name"), d);
        GuiTextures gt = (GuiTextures) pong.getScreen(Constants.textures);
        gt.select.addItem(d.getPropertie("name"), d);
        
        for(int i = 0; i < textures.length; i++)
        {
            if(textures[i].isFile())
            {
                String path = textures[i].getPath();
                if(path.endsWith(".zip"))
                {
                    if(this.isZipAvailable(textures[i]))
                    {
                        File f = new File(this.textureFolder, textures[i].getName().replace(".zip", ""));
                        if(!f.exists())
                        {
                            f.mkdirs();
                        }
                        else
                        {
                            f.delete();
                            f.mkdirs();
                        }
                        
                        FileUtil.unzip(textures[i], f);

                        
                        
                        try {
                            this.addTexturePack(f);
                        } catch (IOException ex) {
                            Logger.getLogger(TextureManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
    }
    
    private void addTexturePack(File f) throws IOException
    {
        TexturePack pack = new TexturePack();
        
        File ball = new File(f.getPath(), "ball.png");
        File wall = new File(f.getPath(), "wall.png");
        File background = new File(f.getPath(), "background.png");
        File player = new File(f.getPath(), "player.png");
        File enemy = new File(f.getPath(), "enemy.png");
        File info = new File(f, "pack.txt");
        
        if(!info.exists() || !ball.exists() || !wall.exists() || !background.exists() || !player.exists())
        {
            Gdx.app.log("[Pong]", "Wrong TexturePack");
        }
        else
        {
            if(Gdx.files.isExternalStorageAvailable())
            {
                pack.setBall(Gdx.files.absolute(ball.getAbsolutePath()));
                pack.setBackground(Gdx.files.absolute(background.getAbsolutePath()));
                pack.setPlayer(Gdx.files.absolute(player.getAbsolutePath()));
                pack.setWall(Gdx.files.absolute(wall.getAbsolutePath()));
            }
            
            if(enemy.exists())
            {
                pack.setEnemy(Gdx.files.absolute(enemy.getAbsolutePath()));
            }
            
            if(pack.setInfo(info))
            {
                this.textureList.put(pack.getPropertie("name"), pack);
                GuiTextures g = (GuiTextures)pong.getScreen(Constants.textures);
                g.select.addItem(pack.getPropertie("name"), pack);
            }
        }
        
        
        
    }
    
    private boolean isZipAvailable(File f)
    {
        try {
            FileInputStream fis = new FileInputStream(f);
            BufferedInputStream bis = new BufferedInputStream(fis);
            ZipInputStream zin = new ZipInputStream(bis);
            ZipEntry ze;
            Gdx.app.log("[Pong]", "IsZipAvailable ...");
            boolean[] files = new boolean[5];
            files[0] = false;
            files[1] = false;
            files[2] = false;
            files[3] = false;
            files[4] = false;
            
            while ((ze = zin.getNextEntry()) != null) 
            {
                if(!ze.isDirectory())
                {
                    String t = ze.getName();
                    if(t.equalsIgnoreCase("ball.png"))
                    {
                        Gdx.app.log("[Pong]", "IsZipAvailable ... " +t);
                        files[0] = true;
                    }
                    else if(t.equalsIgnoreCase("wall.png"))
                    {
                        Gdx.app.log("[Pong]", "IsZipAvailable ... " +t);
                        files[1] = true;
                    }
                    else if(t.equalsIgnoreCase("background.png"))
                    {
                        Gdx.app.log("[Pong]", "IsZipAvailable ... " +t);
                        files[2] = true;
                    }
                    else if(t.equalsIgnoreCase("player.png"))
                    {
                        Gdx.app.log("[Pong]", "IsZipAvailable ... " +t);
                        files[3] = true;
                    }
                    else if(t.equalsIgnoreCase("pack.txt"))
                    {
                        files[4] = true;
                    }
          
                }
            }
            
            if(files[0] && files[1] && files[2] && files[3])
            {
                Gdx.app.log("[Pong]", "IsZipAvailable ... Yes !");
                return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(TextureManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }
    
    
    
    public Texture getPlayer()
    {
        return this.currentTexture.getPlayer();
    }
    
    public Texture getBall()
    {
        return this.currentTexture.getBall();
    }
    
    public Texture getWall()
    {
        return this.currentTexture.getWall();
    }
    public Texture getEnemy()
    {
        return this.currentTexture.getEnemy();
    }
    public Texture getBackground()
    {
        return this.currentTexture.getBackground();
    }
}
