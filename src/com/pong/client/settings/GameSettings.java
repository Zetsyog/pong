/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.pong.client.Pong;
import com.pong.client.util.Util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clem
 */
public class GameSettings 
{
    private Pong pong;
    private File config;
    private boolean update = false;
    
    private HashMap<String, Integer> keys;
    
    public GameSettings(Pong p)
    {
        pong = p;
        keys = new HashMap<String, Integer>();
    }
    
    
    public int getKey(String key)
    {
        int k = (keys.get(key) != null)? keys.get(key) : 0;
        return k;
    }
    public void save() throws IOException
    {
        if(!config.getParentFile().exists())
        {
            config.getParentFile().mkdirs();
        }
        
        if(config.exists())
        {
            config.delete();
            config.createNewFile();
        }
        
        
        PrintWriter pw = new PrintWriter(new FileWriter(config.getPath()));
        for(Entry<String, Integer> e : keys.entrySet())
        {
            
            pw.println(e.getKey() + "= " + Integer.toString(e.getValue()));
        }
        
        pw.close();
    }
    
    
    public void changeKey(String ref, int key)
    {
        if(keys.containsKey(ref))
        {
            keys.remove(ref);
        }
        keys.put(ref, key);
        try {
            this.save();
        } catch (IOException ex) {
            Logger.getLogger(GameSettings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void load()
    {
        if(!pong.getParam("working-dir").equalsIgnoreCase("") && pong.getParam("working-dir") != null)
        {
            config = new File(pong.getParam("working-dir") + "/config/", "config.txt");
            System.out.println(pong.getParam("working-dir"));
            Gdx.app.log("[Pong]", pong.getParam("working-dir"));
        }
        else
        {
            config = new File(Util.getWorkingDirectory("Pong/config").getPath(), "config.txt");
        }
        System.out.println("[Pong] " + config.getPath());
        if(!config.getParentFile().exists())
        {
            config.getParentFile().mkdirs();
        }
        if(!config.exists())
        {
            try {
                config.createNewFile();
                config.setWritable(true);
                config.setReadable(true);
                update = true;
            } catch (IOException ex) {
                Logger.getLogger(GameSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(update)
        {
            try{
                PrintWriter out = new PrintWriter(new FileWriter(config.getPath()));
                out.println("key.up= "+Integer.toString(Input.Keys.UP));
                out.println("key.down= " + Integer.toString(Input.Keys.DOWN));
                
                out.close();
            }
            catch(Exception e){
                Logger.getLogger(GameSettings.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
        try{
            BufferedReader in = new BufferedReader(new FileReader(config.getPath()));
            String line;
            int i = 0;
            this.keys.clear();
            while ((line = in.readLine()) != null) {
                if(line.contains("= "))
                {
                    if(line.startsWith("key"))
                    {
                        this.keys.put(line.split("= ")[0], Integer.parseInt(line.split("= ")[1]));
                    }
                }
            }
            in.close();
        }
        catch(IOException e){
            Logger.getLogger(GameSettings.class.getName()).log(Level.SEVERE, null, e);
        }  
        
        
        
    }
}
