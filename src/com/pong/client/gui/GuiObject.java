/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;


/**
 *
 * @author Clem
 */
abstract public class GuiObject 
{
    protected float x;
    protected float y;
    protected float sx;
    protected ShapeRenderer r;
    protected float sy;
    protected SpriteBatch batch;
    
    public GuiObject(float x, float y, float sx, float sy)
    {
        this.x = x;
        this.y = y;
        this.sx = sx;
        this.sy = sy;
        r = new ShapeRenderer();
        batch = Pong.instance.batch;
    }
    
    abstract public void render(float f);
    
    public float getX()
    {
        return x;
    }
    
    public float getY()
    {
        return y;
    }
    
    public float getSX()
    {
        return sx;
    }
    public float getSY()
    {
        return sy;
    }
    
    
}
