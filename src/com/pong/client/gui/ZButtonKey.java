/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.pong.client.Pong;
import com.pong.client.util.InputUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clem
 */
public class ZButtonKey extends GuiObject implements InputProcessor{
    
    protected String baseKey;
    protected int iKey;
    protected String desc;
    private boolean hover = false;
    private Pong pong;
    private BitmapFont bebas;
    private boolean target = false;
    private BitmapFont gils;
    
    public ZButtonKey(Pong p, int y, String key, String desc)
    {
        super(1, y, 50, 50);
        baseKey = key;
        this.desc = desc;
        pong = p;
        pong.multiplexer.addProcessor(this);
        gils = pong.getFont("gils");
        iKey = pong.gameSettings.getKey(baseKey);
        bebas = pong.getFont("bebas");
    }

    
    @Override
    public void render(float f) 
    {
        this.sx = gils.getBounds(InputUtil.getKeyName(iKey)).width;
        this.sy = gils.getBounds(InputUtil.getKeyName(iKey)).height;
        this.x = Gdx.graphics.getWidth() / 2 + 100;
        if(!target)
        {
            if(pong.getInput().isMouseOver(this))
            {
                hover = true;
                if(Gdx.input.isButtonPressed(Input.Buttons.LEFT))
                {
                    this.target = true;
                }
            }
        }
        
        if(pong.getInput().isMouseOver(this))
        {
            hover = true;
        }
        else
        {
            hover = false;
        }
        
        batch.begin();
            if(hover && !target)
            {
                gils.setColor(Color.DARK_GRAY);
                gils.draw(batch, InputUtil.getKeyName(iKey), x, y);
                //gils.drawString(x, y, Input.getKeyName(iKey), Color.gray);
            }
            else if(!hover && !target)
            {
                gils.setColor(Color.LIGHT_GRAY);
                gils.draw(batch, InputUtil.getKeyName(iKey), x, y);
                //gils.drawString(x, y, Input.getKeyName(iKey), Color.lightGray);

            }
            else if(target)
            {
                gils.setColor(Color.DARK_GRAY);
                gils.draw(batch, InputUtil.getKeyName(iKey), x , y);
                //gils.drawString(x, y, Input.getKeyName(iKey), Color.darkGray);
            }
        bebas.setColor(Color.LIGHT_GRAY);
        bebas.draw(batch, desc + " : ", x - bebas.getBounds(desc).width - 40, y);
        batch.end();
    }
    

    @Override
    public boolean keyDown(int i) {
        return false;
    }

    @Override
    public boolean keyUp(int i) {
        if(target)
        {
            if(!InputUtil.getKeyName(i).equalsIgnoreCase("Unknown"))
            {
                this.iKey = i;
                pong.gameSettings.changeKey(baseKey, iKey);
                try {
                    pong.gameSettings.save();
                } catch (IOException ex) {
                    Logger.getLogger(ZButtonKey.class.getName()).log(Level.SEVERE, null, ex);
                }
                target = false;
                return true;
            }
            else
            {
                target = true;
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
    
    
}
