/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pong.client.Pong;
import com.pong.client.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Clem
 */
public class ZButton extends GuiObject{

    private BitmapFont font;
    private String text;
    public final int id;
    protected final Pong pong;
    private boolean hover = false;
    private ArrayList<ActionListener> listeners;
    private boolean hide;
    private boolean center;
    
    public ZButton(final Pong p, int id, int x, int y, String text)
    {
        super(x - 5, y - 5, 1, 1);
        this.text = text;
        this.id = id;
        pong = p;
        hide = false;
        this.font = pong.getFont("gils");
        this.sx = font.getBounds(this.text).width + 5;
        this.sy = font.getBounds(this.text).height + 5;
        listeners = new ArrayList<ActionListener>();
        center = false;
        
        Gdx.app.debug("[Pong]", Float.toString(x));
        Gdx.app.debug("[Pong]", Float.toString(y));
        Gdx.app.debug("[Pong]", Float.toString(sx));
        Gdx.app.debug("[Pong]", Float.toString(sy));
        
    }
    
    public void setText(String txt)
    {
        text = txt;
    }
    
    public void setCenter(boolean b)
    {
        center = b;
    }
    
    public void show()
    {
        hide = false;
    }
    
    public void hide()
    {
        hide = true;
    }
    
    public void addActionListener(ActionListener ae)
    {
        listeners.add(ae);
    }
    
    public void setFont(BitmapFont font)
    {
        this.font = font;
    }
    
   
    @Override
    public void render(float f) 
    {
        if(center)
        {
            this.x = Gdx.graphics.getWidth() / 2 - font.getBounds(text).width / 2;
        }
        if(!hide)
        {
            hover = pong.getInput().isMouseOver(this);
            if(hover)
            {
                if(Gdx.input.isButtonPressed(Input.Buttons.LEFT) && !hide)
                {
                    for(ActionListener ae : listeners)
                    {
                        ae.actionPerformed(this, pong);
                    }
                }
            }
            pong.batch.flush();
            pong.batch.begin();
                if(hover)
                {
                    font.setColor(Color.DARK_GRAY);
                }
                else
                {
                    font.setColor(Color.LIGHT_GRAY);
                }
                font.draw(pong.batch, text, x, y);

            pong.batch.end();
            pong.batch.flush();
            if(hover)
            {
                r.begin(ShapeRenderer.ShapeType.Line);
                    r.setColor(Color.DARK_GRAY);
                    r.line(x, y - sy - 5, x + sx, y - sy - 5);
                r.end();            
            }
        }
    }
    
}
