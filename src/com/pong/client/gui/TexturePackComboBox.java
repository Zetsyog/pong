/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.screen.GuiInGame;
import com.pong.client.texture.TexturePack;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author Clem
 */
public class TexturePackComboBox extends GuiObject{

    private Pong pong;
    private Stage stage;
    private BitmapFont bebas;
    private Table table;
    private Table scrollTable;
    private ScrollPane scroll;
    private HashMap<String, CheckBox> checks;
    private LabelStyle style;
    private CheckBoxStyle cstyle;
    private LabelStyle s1;
    private LabelStyle s2;
    
    private HashMap<String, TexturePack> items;
    public TexturePackComboBox(Pong p, int x, int y, int sx, int sy)
    {
        super(x, y, sx, sy);
        System.out.println("sx : " + sx + " sy : " + sy);
        pong = p;
        checks = new HashMap<String, CheckBox>();
        bebas = pong.getFont("bebas");
        items = new HashMap<String, TexturePack>();
        stage = new Stage();
        
        cstyle = pong.styleManager.checkBox();
        pong.multiplexer.addProcessor(stage);
        
        
        style = new LabelStyle();
        style.font = pong.getFont("bebas-little");
        
        scrollTable = new Table();
        scrollTable.setWidth(sx);
        scrollTable.setHeight(sy);
        scrollTable.align(Align.center);
        
        scroll = new ScrollPane(scrollTable, pong.styleManager.scrollStyle());
        scroll.setPosition(x, y);
        scroll.setWidth(sx);
        scroll.setHeight(sy);
        
        stage.addActor(scroll);
        stage.setScrollFocus(scroll);
        
        s1 = style;
        s1.fontColor = Color.DARK_GRAY;
        
        s2 = style;
        s2.fontColor = Color.DARK_GRAY;
    }
    public void addItem(String key, TexturePack e)
    {
        this.items.put(key, e);
        String name = e.getPropertie("name");
        String author = e.getPropertie("author");
        String desc = e.getPropertie("description");
        
        Label titre = new Label(name + " by " + author, s1);
        titre.setAlignment(Align.center);
        
        Label ldesc = new Label(desc, s2);
        ldesc.setAlignment(Align.center);
        
        CheckBox b = new CheckBox("Utiliser \n", cstyle);
        b.size(32, 32);
        b.setWidth(32);
        b.setHeight(32);
        if(e.getPropertie("name").equalsIgnoreCase("default"))
        {
            b.setChecked(true);
        }
        
        scrollTable.add(titre);
        scrollTable.row();
        scrollTable.add(ldesc);
        scrollTable.row();
        scrollTable.add(b);
        scrollTable.row();
        scrollTable.add(new Label("_____________________________\n", style));
        scrollTable.row();
        checks.put(e.getPropertie("name"), b);
        
    }
    
    @Override
    public void render(float f) 
    {
        stage.act(f);
        scroll.act(f);
        stage.draw();
        batch.begin();
            
        batch.end();
        
        for(Entry<String, CheckBox> e : checks.entrySet())
        {
            CheckBox b = e.getValue();
            if(b.getClickListener().isPressed() && !b.isChecked())
            {
                for(Entry<String, CheckBox> c : checks.entrySet())
                {
                    if(!c.getKey().equalsIgnoreCase(e.getKey()))
                    {
                        c.getValue().setChecked(false);
                    }
                }
                GuiInGame g = (GuiInGame) pong.getScreen(Constants.game); 
                g.setTexturePack(this.items.get(e.getKey()));
            }
            
            if(b.getClickListener().isPressed() && b.isChecked())
            {
                b.getClickListener().cancel();
                b.setChecked(true);
            }
        }
        /*this.r.begin(ShapeRenderer.ShapeType.Filled);
            r.setColor(Color.BLACK);
            r.rect(x, y, sx, sy);
        this.r.end();*/
    }
    
    public void dispose()
    {
        stage.dispose();
    }


}
