/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.event.ActionListener;
import com.pong.client.gui.ZButton;
import com.pong.client.gui.ZButtonKey;

/**
 *
 * @author Clem
 */
class GuiOptions extends GuiScreen implements ActionListener{

    private int menuY = 350;
    private ZButton back;
    private ZButtonKey up;
    private ZButtonKey down;
    
    public GuiOptions(Pong pong, int settings) 
    {
        super(pong, settings);
        
        
        up = new ZButtonKey(pong, menuY, "key.up", "Se deplacer vers le haut");
        down = new ZButtonKey(pong, menuY - 80, "key.down", "Se deplacer vers le bas");
        
        back = new ZButton(pong, 0, 20, 50, "Retour");
        back.addActionListener(this);
        
        gui.add(up);
        gui.add(down);
        gui.add(back);
    }


    @Override
    public void render(float i)
    {
        float cx = Gdx.graphics.getWidth() / 2;
        super.render(i);
        batch.begin();
            gils.setColor(Color.LIGHT_GRAY);
            gils.draw(batch, "Options :", cx - gils.getBounds("Options :").width / 2, menuY + 150);
        batch.end();
    }
    
    @Override
    public void actionPerformed(ZButton button, Pong pong)
    {
        pong.setScreen(pong.getScreen(Constants.mainMenu));
    }
    
}
