/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.event.ActionListener;
import com.pong.client.gui.ZButton;
import com.pong.client.gui.TexturePackComboBox;

/**
 *
 * @author Clem
 */
public class GuiTextures extends GuiScreen implements ActionListener{
    
    private ZButton back;
    public TexturePackComboBox select;
    private BitmapFont bebas;
    
    public GuiTextures(Pong p, int i)
    {
        super(p, i);
        
        back = new ZButton(pong, 0, 50, 80, "Retour");
        back.addActionListener(this);
        select = new TexturePackComboBox(pong, 200, 100, 400, 400);
        
        bebas = pong.getFont("bebas");
        
        gui.add(back);
        gui.add(select);
    }
    
    @Override
    public void render(float i)
    {
        super.render(i);
        
        batch.begin();
            bebas.setColor(Color.DARK_GRAY);
            this.bebas.draw(batch, "Texture Pack (beta)", Gdx.graphics.getWidth() / 2 - bebas.getBounds("Texture Pack (beta)").width / 2, 550);
        batch.end();
    }

    @Override
    public void actionPerformed(ZButton button, Pong pong) 
    {
        if(button.id == 0)
        {
            pong.setScreen(pong.getScreen(Constants.mainMenu));
        }
    }
}
