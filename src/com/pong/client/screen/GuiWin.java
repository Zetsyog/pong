/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.event.ActionListener;
import com.pong.client.gui.ZButton;

/**
 *
 * @author Clem
 */
public class GuiWin extends GuiScreen
{
    private Texture logo;
    private int menuY = 260;
    protected BitmapFont bebas;
    private ZButton retry;
    private ZButton back;
    
    public GuiWin(final Pong p, int i)
    {
        super(p, i);
        
        bebas = pong.getFont("bebas");
        logo = new Texture(Gdx.files.local("ressource/image/logo.png"));
        
        back = new ZButton(pong, 0, 50, menuY - 100, "Retour au Menu");
        back.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ZButton button, Pong pong) {
                pong.setScreen(pong.getScreen(Constants.mainMenu));
            }
        });
        
        retry = new ZButton(pong, 0, 50, menuY, "Réessayer");
        retry.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ZButton button, Pong pong) {
                pong.setScreen(pong.getScreen(Constants.game));
            }
        });
        
        back.setCenter(true);
        retry.setCenter(true);
        
        gui.add(back);
        gui.add(retry);
    }
    
    @Override
    public void render(float f)
    {
        super.render(f);
        
        batch.begin();
            batch.draw(logo, Gdx.graphics.getWidth() / 2 - logo.getWidth() / 2, menuY + 100);
            bebas.setColor(Color.LIGHT_GRAY);
            bebas.draw(batch, "Victoire !!", Gdx.graphics.getWidth() / 2 - bebas.getBounds("Victoire !!").width / 2, menuY + 80);
        batch.end();
    }
    
}
