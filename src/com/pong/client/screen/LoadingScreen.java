/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.pong.client.Constants;
import com.pong.client.Pong;

/**
 *
 * @author Clem
 */
public class LoadingScreen extends GuiScreen{
    
    private Texture img;
    private int first = 0;
    
    public LoadingScreen(final Pong p, int id)
    {
        super(p, id);
        img = new Texture(Gdx.files.local("ressource/image/loading-logo.png"));
    }
    
    public void init()
    {
        /*Texture texture = new Texture(Gdx.files.internal("ressource/font/gils.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        BitmapFont font = new BitmapFont(Gdx.files.internal("ressource/font/gils.fnt"), new TextureRegion(texture), false);
        pong.addFont("gils", font);*/
        
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("ressource/font/gilsanub.ttf"));
        BitmapFont font = generator.generateFont(40); // font size 12 pixels
        pong.addFont("gils", font);
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
        
        
        /*Texture texture2 = new Texture(Gdx.files.internal("ressource/font/bebas.png"));
        texture2.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        BitmapFont font2 = new BitmapFont(Gdx.files.internal("ressource/font/bebas.fnt"), new TextureRegion(texture2), false);
        pong.addFont("bebas", font2);*/
        
        FreeTypeFontGenerator generator2 = new FreeTypeFontGenerator(Gdx.files.internal("ressource/font/bebas.ttf"));
        BitmapFont font2 = generator2.generateFont(32); // font size 12 pixels
        BitmapFont font3 = generator2.generateFont(20);
        pong.addFont("bebas-little", font3);
        pong.addFont("bebas", font2);
        generator2.dispose(); // don't forget to dispose to avoid memory leaks!
        
        pong.styleManager.load();
        
        
        pong.addScreen(new GuiMainMenu(pong, Constants.mainMenu));
        pong.addScreen(new GuiTextures(pong, Constants.textures));
        pong.loadTM();
        pong.addScreen(new GuiInGame(pong, Constants.game));
        pong.addScreen(new GuiWin(pong, Constants.win));
        pong.addScreen(new GuiLose(pong, Constants.lose));
        pong.addScreen(new GuiOptions(pong, Constants.settings));
       
        
        
        
        pong.setScreen(pong.getScreen(Constants.mainMenu));
    }
    
    @Override
    public void render(float f)
    {
        batch.begin();
            batch.draw(img, 0, 0);
        batch.end();
        
        if(first >= 80)
        {
            this.init();
        }
        else
        {
            first++;
        }
    }
}
