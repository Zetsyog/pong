/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.event.ActionListener;
import com.pong.client.gui.ZButton;

/**
 *
 * @author Clem
 */
public class GuiMainMenu extends GuiScreen implements ActionListener
{
    private ZButton play;
    private ZButton quit;
    private ZButton option;
    private ZButton texture;
    private int menuX = 500;
    private int menuY = 400;
    private TextureRegion logo;
    public GuiMainMenu(final Pong p, int id)
    {
        super(p, id);
        play = new ZButton(pong, 0, menuX, menuY, "Jouer");
        play.addActionListener(this);
        
        option = new ZButton(pong, 2, menuX, menuY - 80, "Options");
        option.addActionListener(this);
        
        texture = new ZButton(pong, 3, 80, menuY - 80 * 2, "Texture Pack");
        texture.addActionListener(this);
        
        quit = new ZButton(pong, 1, menuX, menuY - 80 * 2, "Quitter");
        quit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ZButton button, Pong pong) 
            {
                Gdx.app.exit();
            }
        });
        Texture tex = new Texture(Gdx.files.local("ressource/image/logo.png"));
        logo = new TextureRegion(tex, 0, 0, 433, 184);
        gui.add(play);
        gui.add(option);
        gui.add(texture);
        gui.add(quit);
    }
    
    @Override
    public void render(float i)
    {
        super.render(i);
        
        batch.begin();
            batch.draw(logo, 20, 270);
        batch.end();
    }

    @Override
    public void actionPerformed(ZButton button, Pong pong) 
    {
        if(button.id == 0)
        {
            /* WIP */
            pong.setScreen(pong.getScreen(Constants.game));
        }
        else if(button.id == 2)
        {
            pong.setScreen(pong.getScreen(Constants.settings));
        }
        else if(button.id == 3)
        {
            pong.setScreen(pong.getScreen(Constants.textures));
        }
    }
}
