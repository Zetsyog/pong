/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pong.client.Pong;
import com.pong.client.gui.GuiObject;
import java.util.ArrayList;

/**
 *
 * @author Clem
 */
abstract public class GuiScreen implements Screen
{
    protected final Pong pong;
    private Texture bg;
    protected ArrayList<GuiObject> gui;
    private int id;
    protected SpriteBatch batch;
    protected BitmapFont gils;
    
    public GuiScreen(final Pong p, int id)
    {
        pong = p;
        this.id = id;
        gils = pong.getFont("gils");
        gui = new ArrayList<GuiObject>();
        bg = new Texture(Gdx.files.internal("ressource/image/background.png"));
        batch = pong.batch;
    }
    
    public int getId()
    {
        return id;
    }
    
    @Override
    public void render(float f) 
    {
        batch.flush();
        batch.begin();
        batch.draw(bg, 0, 0);
        batch.end();
        batch.flush();
        
        for(GuiObject go : gui)
        {
            go.render(f);
        }
    }

    @Override
    public void resize(int i, int i1) 
    {
        
    }

    @Override
    public void show() 
    {
        
    }

    @Override
    public void hide() 
    {
        
    }

    @Override
    public void pause() 
    {
        
    }

    @Override
    public void resume() 
    {
        
    }

    @Override
    public void dispose() 
    {
        
    }
    
}
