/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pong.client.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.pong.client.Constants;
import com.pong.client.Pong;
import com.pong.client.engine.GameEngine;
import com.pong.client.entity.*;
import com.pong.client.event.ActionListener;
import com.pong.client.gui.GuiObject;
import com.pong.client.gui.ZButton;
import com.pong.client.texture.TexturePack;
import com.pong.client.util.Timer;
import java.util.ArrayList;

/**
 *
 * @author Clem
 */
public class GuiInGame extends GuiScreen {
    
    protected ArrayList<Entity> entities;
    private EntityPlayer player;
    private EntityPlayer enemy;
    private EntityBall ball;
    private Texture bg;
    private ZButton back;
    private ZButton backToGame;
    private GameEngine ge;
    private EntityWall wall1;
    private EntityWall wall2;
    private BitmapFont bebas;
    private Texture black;
    
    private Texture logo;
    private int menuY = 200;
    private boolean pause;
    private Timer timer;
    
    public GuiInGame(Pong p, int id)
    {
        super(p, id);
        
        entities = new ArrayList<Entity>();
        ball = new EntityBall(pong, Gdx.graphics.getWidth() / 2 - EntityBall.SIZE / 2, Gdx.graphics.getHeight() / 2 - EntityBall.SIZE / 2);
        player = new EntityPlayer(pong, 1, Gdx.graphics.getHeight() / 2 - EntityPlayer.SIZEY / 2, ball, pong.getParam("username"), false);
        
        enemy = new EntityPlayer(pong, Gdx.graphics.getWidth() - EntityPlayer.SIZEX, Gdx.graphics.getHeight() / 2 - EntityPlayer.SIZEY / 2, ball,"Ennemi", true);
        wall1 = new EntityWall(pong, 0,Gdx.graphics.getHeight() - EntityWall.STDSIZE, ball);
        wall2 = new EntityWall(pong, 0,0,ball);
        
        bebas = pong.getFont("bebas");
        pause = false;
        timer = new Timer();
        
        bg = new Texture(Gdx.files.local("ressource/image/background.png"));
        
        ge = new GameEngine(ball, player, enemy, wall1, wall2);
        back = new ZButton(pong, 0, 50, menuY, "Retour au Menu");
        back.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ZButton button, Pong pong) {
                pong.setScreen(pong.getScreen(Constants.mainMenu));
                GuiInGame.this.reset();
            }
        });
        
        this.backToGame = new ZButton(pong, 1, 200, menuY + 70, "Retour au jeu");
        this.backToGame.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ZButton button, Pong pong) {
                GuiInGame.this.pause = false;
                backToGame.hide();
                back.hide();
            }
        });
        black = new Texture(Gdx.files.local("ressource/image/dark.png"));
        logo = new Texture(Gdx.files.local("ressource/image/logo.png"));
        
        back.setCenter(true);
        backToGame.setCenter(true);
        
        backToGame.hide();
        back.hide();
        
        /*entities.add(player);
        entities.add(enemy);
        entities.add(ball);
        entities.add(wall1);
        entities.add(wall2);*/
        
        gui.add(back);
        gui.add(this.backToGame);
    }
    
    public void setTexturePack(TexturePack p)
    {
        this.enemy.setTexturePack(p);
        player.setTexturePack(p);
        wall1.setTexturePack(p);
        wall2.setTexturePack(p);
        ball.setTexturePack(p);
        this.bg = p.getBackground();
    }
    
    
    
    
    public void reset()
    {
        ge.reset();
        pause = false;
        backToGame.hide();
        back.hide();
        timer.reset();
    }
    
    public void setMaxScore(int max)
    {
        ge.setMaxScore(max);
    }
    
    @Override
    public void render(float f)
    {
        batch.begin();
            batch.draw(bg, 0, 0);
        batch.end();
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
        {
            if(!pause)
            {
                pause = true;
                backToGame.show();
                back.show();
            }
        }
        
        if(!pause)
        {
            ge.update(f);
        }
        ge.render(f);
        
        for(GuiObject go : gui)
        {
            go.render(f);
        }
        
        timer.setPause(pause);
        
        timer.update(f);
        batch.flush();
        batch.begin();
            bebas.setColor(Color.GREEN);
            bebas.draw(batch, this.player.getName() + " : " + this.ge.getPlayerScore(), 20, Gdx.graphics.getHeight() - 20);
            bebas.setColor(Color.RED);
            String text = this.enemy.getName() + " : " + this.ge.getEnemyScore();
            bebas.draw(batch, text, Gdx.graphics.getWidth() - bebas.getBounds(text).width - 20, Gdx.graphics.getHeight() - 20);
            
            bebas.setColor(Color.WHITE);
            bebas.draw(batch, timer.getMin() + ":" + timer.getSec()  + ","+ timer.getDix(), 700, 50);
            
            // Si le jeu est en pause on dessine le fond noir semi-transparant (donne l'effet foncé)
            if(pause)
            {
                // Méthode qui permet de répéter l'image
                black.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
                batch.draw(black, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                batch.draw(logo, Gdx.graphics.getWidth() / 2 - logo.getWidth() / 2, menuY + 100);
            }
        batch.end();
        batch.flush();
        
        
    }
    
    @Override
    public void dispose()
    {
        ge.dispose();
    }
}
